package com.example.smokestalks

import android.app.Application
import android.content.Context
import com.example.smokestalks.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class SmokesAndTalks : Application() {

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@SmokesAndTalks)
            modules(
                listOf(
                    presenterModule,
                    networkModule,
                    repositoryModule,
                    sharedPreferencesModule,
                    databaseModule
                )
            )
        }
        Timber.plant(Timber.DebugTree())
    }

    companion object {
        private var instance: SmokesAndTalks? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }
}

package com.example.smokestalks.entities.response

import com.google.gson.annotations.SerializedName

data class AddCartItemResponse(
    @SerializedName("newCartItem")
    val newCartItem : NewCartItem
)

data class NewCartItem(
    @SerializedName("_id")
    val id : String,
    @SerializedName("cart_id")
    val cartId : String,
    @SerializedName("product")
    val productId : String
)

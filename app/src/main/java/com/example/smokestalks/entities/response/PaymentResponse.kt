package com.example.smokestalks.entities.response

import com.google.gson.annotations.SerializedName

data class PaymentResponse(
    val result: String,
    @SerializedName("payment_id")
    val paymentId: String,
    val action: String,
    val status: String,
    val version: Int,
    val type: String,
    val paytype: String,
    @SerializedName("public_key")
    val publicKey: String,
    @SerializedName("acq_id")
    val acqId: String,
    @SerializedName("order_id")
    val orderId: String,
    @SerializedName("liqpay_order_id")
    val liqpayOrderId: String,
    val description: String,
    @SerializedName("sender_phone")
    val senderPhone: String,
    @SerializedName("sender_card_mask2")
    val senderCardMask2: String,
    @SerializedName("sender_card_bank")
    val senderCardBank: String,
    @SerializedName("sender_card_type")
    val senderCardType: String,
    @SerializedName("sender_card_country")
    val senderCardCountry: Int,
    val amount: Int,
    val currency: String,
    @SerializedName("sender_commission")
    val senderCommission: Float,
    @SerializedName("receiver_commission")
    val receiverCommission: Float,
    @SerializedName("agent_commission")
    val agentCommission: Float,
    @SerializedName("amount_debit")
    val amountDebit: Float,
    @SerializedName("amount_credit")
    val amountCredit: Float,
    @SerializedName("commission_debit")
    val commissionDebit: Float,
    @SerializedName("commission_credit")
    val commissionCredit: Float,
    @SerializedName("currency_debit")
    val currencyDebit: String,
    @SerializedName("currency_credit")
    val currencyCredit: String,
    @SerializedName("sender_bonus")
    val senderBonus: Float,
    @SerializedName("amount_bonus")
    val amountBonus: Float,
    @SerializedName("mpi_eci")
    val mpiEci: String,
    @SerializedName("is_3ds")
    val is3ds: Boolean,
    @SerializedName("create_date")
    val createDate: Long,
    @SerializedName("end_date")
    val endDate: Long,
    @SerializedName("confirm_token")
    val confirmToken: String,
    @SerializedName("transaction_id")
    val transactionId: String,
)

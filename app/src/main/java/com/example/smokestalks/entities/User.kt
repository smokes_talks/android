package com.example.smokestalks.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class User(
    @PrimaryKey
    val uid: String,
    val phone: String,
    var name: String?,
    var surname: String?,
    var email: String?,
    var birthday: Date?,
    var gender: String?,
    var city: String?
)
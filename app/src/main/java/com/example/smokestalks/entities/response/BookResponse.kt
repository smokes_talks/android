package com.example.smokestalks.entities.response

data class BookResponse(
    val adults: Int,
    val children: Int,
    val readyBefore: String,
    val date: String
)

package com.example.smokestalks.entities

data class MenuItem (
    val id : String,
    val title : String,
    val categoryId : Int,
    val subcategoryId : Int,
    val price : Int,
    val description : String,
    val image : String,
    val size : String
)


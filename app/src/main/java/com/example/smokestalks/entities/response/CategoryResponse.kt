package com.example.smokestalks.entities.response

import com.google.gson.annotations.SerializedName

data class CategoryResponse (
    @SerializedName("_id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("subcategories")
    val subcategories: List<SubcategoryResponse>
)
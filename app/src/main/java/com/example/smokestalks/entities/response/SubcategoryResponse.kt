package com.example.smokestalks.entities.response

import com.google.gson.annotations.SerializedName

data class SubcategoryResponse(
    @SerializedName("_id")
    val id: Int,

    @SerializedName("name")
    val name: String
)
package com.example.smokestalks.entities.request

import com.google.gson.annotations.SerializedName

data class ProfileRequest (
    @SerializedName("first_name")
    val firstName : String,
    @SerializedName("last_name")
    val lastName : String,
    @SerializedName("email")
    val email : String,
    @SerializedName("birth_date")
    val birthday : String,
    @SerializedName("gender")
    val gender : String
)
package com.example.smokestalks.entities.response

data class RegisteredResponse (
    val registered : Boolean
)
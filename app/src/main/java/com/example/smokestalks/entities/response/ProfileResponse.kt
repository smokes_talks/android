package com.example.smokestalks.entities.response

import com.google.gson.annotations.SerializedName

data class ProfileResponse (
    @SerializedName("uid")
    val uid : String,
    @SerializedName("first_name")
    val firstName : String,
    @SerializedName("last_name")
    val lastName : String,
    @SerializedName("email")
    val email : String,
    @SerializedName("birth_date")
    val birthday : String,
    @SerializedName("gender")
    val gender : String,
    @SerializedName("profile_image")
    val profileImage : String
)
package com.example.smokestalks.entities

data class Category(
    val id: Int,
    val name: String
)
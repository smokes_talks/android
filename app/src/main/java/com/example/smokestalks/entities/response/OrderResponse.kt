package com.example.smokestalks.entities.response

import com.google.gson.annotations.SerializedName

data class OrderResponse(
    @SerializedName("_id")
    val orderId: String,
    @SerializedName("total")
    val totalOrderSum: Price,
    @SerializedName("table_id")
    val tableId: Int,
    val date: String
)

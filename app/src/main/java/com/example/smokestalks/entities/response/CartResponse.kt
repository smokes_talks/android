package com.example.smokestalks.entities.response

import com.google.gson.annotations.SerializedName

data class CartResponse(
    @SerializedName("id")
    val cartId : String,
    @SerializedName ("total")
    val total: Price,
    @SerializedName("items")
    val cartItems : List<CartItem>
)

data class CartItem(
    @SerializedName("_id")
    val id : String,
    @SerializedName("cart_id")
    val cartId : String,
    @SerializedName("product")
    val product : MenuItemResponse,
    @SerializedName("quantity")
    val quantity : Int
)

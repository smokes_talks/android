package com.example.smokestalks.entities.response

import com.google.gson.annotations.SerializedName

data class MenuItemResponse (
    @SerializedName("_id")
    val id : String,
    val title : String,
    val category: String,
    val subcategory : String,
    val price: Price,
    val description: String,
    val image: String,
    val size: String?
)

data class Price(
    @SerializedName("\$numberDecimal")
    val numberDecimal : String
)
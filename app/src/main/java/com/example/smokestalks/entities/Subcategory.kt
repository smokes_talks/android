package com.example.smokestalks.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

data class Subcategory(
    val id: Int,
    val name: String,
    val categoryId: Int
)
package com.example.smokestalks.repositories

import androidx.lifecycle.MutableLiveData
import com.example.smokestalks.api.APIService
import com.example.smokestalks.entities.MenuItem
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class OrderRepository(
    private val service: APIService,
    private val userRepository: UserRepository,
    private val menuRepository: MenuRepository
) : CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    var sumOrder: MutableLiveData<Int> = MutableLiveData(0)

    // товар и количество
    private val cart = mutableMapOf<MenuItem, Int>()

    fun getCartItems() = cart.keys.toMutableList()

    fun isCartEmpty() = cart.isEmpty()

    fun getCountOfMenuItem(menuItem: MenuItem) = cart[menuItem] ?: 0

    fun addCartItem(menuItem: MenuItem) {
        cart[menuItem] = 1
        updateSumOrder()
        launch {
            try {
                service.addCartItem(
                    userRepository.getAuthorizedUserIdToken(),
                    menuItem.id
                )
            } catch (e: retrofit2.HttpException) {
                userRepository.updateIdToken()
                service.addCartItem(
                    userRepository.getAuthorizedUserIdToken(),
                    menuItem.id
                )
            }
        }
    }

    fun removeCartItem(menuItem: MenuItem) {
        cart.remove(menuItem)
        updateSumOrder()
        launch {
            try {
                service.removeCartItem(
                    userRepository.getAuthorizedUserIdToken(),
                    menuItem.id
                )
            } catch (e: retrofit2.HttpException) {
                userRepository.updateIdToken()
                service.removeCartItem(
                    userRepository.getAuthorizedUserIdToken(),
                    menuItem.id
                )
            }
        }
    }

    fun changeCartItemQuantity(menuItem: MenuItem, count: Int) {
        cart[menuItem] = count
        updateSumOrder()
        launch {
            try {
                service.changeCartItemQuantity(
                    userRepository.getAuthorizedUserIdToken(),
                    menuItem.id,
                    count
                )
            } catch (e: retrofit2.HttpException) {
                userRepository.updateIdToken()
                service.changeCartItemQuantity(
                    userRepository.getAuthorizedUserIdToken(),
                    menuItem.id,
                    count
                )
            }
        }
    }

    private fun updateSumOrder() {
        var newSumOrder = 0
        for (d in cart) {
            newSumOrder += d.key.price * d.value
        }
        sumOrder.value = newSumOrder
    }

    suspend fun loadCart() {
        if (cart.isEmpty()) {
            cart.clear()
            val cartResponse = try {
                service.getCart(userRepository.getAuthorizedUserIdToken())
            } catch (e: retrofit2.HttpException) {
                userRepository.updateIdToken()
                service.getCart(userRepository.getAuthorizedUserIdToken())
            }
            for (cartItem in cartResponse.cartItems) {
                val product = MenuItem(
                    cartItem.product.id,
                    cartItem.product.title,
                    menuRepository.getCategoryByName(cartItem.product.category)!!.id,
                    menuRepository.getSubcategoryByName(cartItem.product.subcategory)!!.id,
                    cartItem.product.price.numberDecimal.toInt(),
                    cartItem.product.description,
                    cartItem.product.image,
                    cartItem.product.size ?: ""
                )
                cart[product] = cartItem.quantity
            }
            withContext(Dispatchers.Main) {
                sumOrder.value = cartResponse.total.numberDecimal.toInt()
            }
        }
    }

    fun clearCart() {
        cart.clear()
        sumOrder.value = 0
    }
}
package com.example.smokestalks.repositories

import androidx.lifecycle.MutableLiveData
import com.example.smokestalks.entities.Category
import com.example.smokestalks.entities.MenuItem
import com.example.smokestalks.entities.Subcategory
import com.example.smokestalks.api.APIService

class MenuRepository(private val service: APIService) {

    var chosenCategoryId = 2
    var chosenSubcategoryId = -1
    private var categoryList = mutableListOf<Category>()

    // ключ - id категории
    private var subcategoryMap = mutableMapOf<Int, MutableList<Subcategory>>()

    private var menuList = mutableListOf<MenuItem>()

    suspend fun getCategories(): MutableList<Category> {
        return if (categoryList.isEmpty()) {
            loadCategories()
            categoryList
        } else
            categoryList
    }

    suspend fun getSubcategoryListByCategoryId(categoryId: Int): MutableList<Subcategory> {
        return if (subcategoryMap.isEmpty()) {
            loadCategories()
            subcategoryMap[categoryId]!!
        } else
            subcategoryMap[categoryId]!!
    }

    suspend fun loadCategories() {
        if (categoryList.isEmpty()) {
            val categoryResponseList = service.getCategories()
            for (category in categoryResponseList) {
                categoryList.add(Category(category.id, category.name))
                subcategoryMap[category.id] = mutableListOf()
                for (subcategory in category.subcategories) {
                    subcategoryMap[category.id]!!.add(
                        Subcategory(
                            subcategory.id,
                            subcategory.name,
                            category.id
                        )
                    )
                }
            }
        }
    }

    fun getCategoryByName(name: String): Category? {
        for (category in categoryList) {
            if (category.name == name)
                return category
        }
        return null
    }

    fun getSubcategoryByName(name: String): Subcategory? {
        for (subcategoryList in subcategoryMap.values) {
            for (subcategory in subcategoryList) {
                if (subcategory.name == name)
                    return subcategory
            }
        }
        return null
    }

    suspend fun getMenuListBySubcategoryId(subcategoryId: Int): MutableList<MenuItem> {
        val list = arrayListOf<MenuItem>()
        return if (menuList.isEmpty()) {
            loadMenu()
            for (menuItem in menuList) {
                if (menuItem.subcategoryId == subcategoryId)
                    list.add(menuItem)
            }
            list
        } else {
            for (menuItem in menuList) {
                if (menuItem.subcategoryId == subcategoryId)
                    list.add(menuItem)
            }
            list
        }
    }

    suspend fun getMenuListByCategoryId(categoryId: Int): MutableList<MenuItem> {
        val list = arrayListOf<MenuItem>()
        return if (menuList.isEmpty()) {
            loadMenu()
            for (menuItem in menuList) {
                if (menuItem.categoryId == categoryId)
                    list.add(menuItem)
            }
            list
        } else {
            for (menuItem in menuList) {
                if (menuItem.categoryId == categoryId)
                    list.add(menuItem)
            }
            list
        }
    }

    suspend fun loadMenu() {
        if (menuList.isEmpty()) {
            for (category in getCategories()) {
                for (subcategory in getSubcategoryListByCategoryId(category.id)) {
                    val menuListResponse = service.getMenuItem(category.id, subcategory.id)
                    for (menuItemResponse in menuListResponse) {
                        val menuItem = MenuItem(
                            menuItemResponse.id,
                            menuItemResponse.title,
                            getCategoryByName(menuItemResponse.category)!!.id,
                            getSubcategoryByName(menuItemResponse.subcategory)!!.id,
                            menuItemResponse.price.numberDecimal.toInt(),
                            menuItemResponse.description,
                            menuItemResponse.image,
                            menuItemResponse.size ?: ""
                        )
                        menuList.add(menuItem)
                    }
                }
            }
        }
    }
}
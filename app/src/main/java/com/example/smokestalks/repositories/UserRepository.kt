package com.example.smokestalks.repositories

import android.content.SharedPreferences
import com.example.smokestalks.database.AppDatabase
import com.example.smokestalks.entities.User
import com.example.smokestalks.api.APIService
import com.example.smokestalks.entities.request.ProfileRequest
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlinx.coroutines.tasks.await
import kotlin.coroutines.CoroutineContext

class UserRepository(
    private val appDatabase: AppDatabase,
    private val sharedPreferences: SharedPreferences,
    private val apiService: APIService
){

    private val mAuth = FirebaseAuth.getInstance()
    var currentUser: User? = null

    fun getAuthorizedUserIdToken() = sharedPreferences.getString(
        "authorizedUserIdToken",
        ""
    )!!

    suspend fun loadCurrentUser() {
        currentUser = appDatabase.userDao().getUserByUid(mAuth.currentUser!!.uid)!!
    }

    fun setAuthorizedUserIdToken(idToken: String) {
        sharedPreferences.edit().putString("authorizedUserIdToken", idToken).apply()
    }

    fun clearAuthorizedUserIdToken() {
        sharedPreferences.edit().putString("authorizedUserIdToken", "").apply()
    }

    suspend fun insertUserToDatabase(user: User) = appDatabase.userDao().insertUser(user)

    suspend fun getUserByUid(uid: String): User? = appDatabase.userDao().getUserByUid(uid)

    suspend fun updateUserInDatabase(user: User) = appDatabase.userDao().updateUser(user)

    suspend fun createUserProfileOnServer(user: User) {
        val idToken = getAuthorizedUserIdToken()
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
        var birthdayString = ""
        if (user.birthday != null)
            birthdayString = formatter.format(user.birthday!!)
        val profileRequest = ProfileRequest(
            user.name!!,
            user.surname!!,
            user.email!!,
            birthdayString,
            user.gender!!
        )
        try {
            apiService.createUserProfile(idToken, profileRequest)
        } catch (e: retrofit2.HttpException) {
            updateIdToken()
            apiService.createUserProfile(getAuthorizedUserIdToken(), profileRequest)

        }
    }

    suspend fun getCurrentUserFromServer(): User {
        val idToken = getAuthorizedUserIdToken()
        val userResponse = apiService.getUserProfileByIdToken(idToken)
        var birthdayDate = Date()
        if (!userResponse.birthday.isNullOrEmpty()) {
            val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
            birthdayDate = parser.parse(userResponse.birthday)!!
        }
        return User(
            userResponse.uid,
            mAuth.currentUser!!.phoneNumber!!,
            userResponse.firstName,
            userResponse.lastName,
            userResponse.email,
            birthdayDate,
            userResponse.gender,
            "Одесса"
        )
    }

    suspend fun isCurrentUserRegistered(): Boolean {
        val idToken = getAuthorizedUserIdToken()
        val registeredResponse = apiService.isUserRegistered(idToken)
        return registeredResponse.registered
    }

    suspend fun updateIdToken() {
        mAuth.currentUser!!.getIdToken(true)
            .addOnSuccessListener { tokenResult ->
                val newIdToken = tokenResult.token!!
                setAuthorizedUserIdToken(newIdToken)
                Timber.d("Refreshed Token: ${getAuthorizedUserIdToken()}")
            }.await()
    }

    fun isLoginAnonymous(): Boolean {
        return sharedPreferences.getBoolean("isLoginAnonymous", false)
    }

    fun setAnonymousLogin() {
        sharedPreferences.edit().putBoolean("isLoginAnonymous", true).apply()
    }

    fun setAnonymousLoginToFalse() {
        sharedPreferences.edit().putBoolean("isLoginAnonymous", false).apply()
    }
}
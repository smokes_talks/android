package com.example.smokestalks.api

import com.example.smokestalks.entities.request.ProfileRequest
import com.example.smokestalks.entities.response.*
import retrofit2.http.*

interface APIService {

    @GET("categories")
    suspend fun getCategories(): List<CategoryResponse>

    @GET("menu/{categoryId}/{subcategoryId}")
    suspend fun getMenuItem(
        @Path("categoryId") categoryId: Int,
        @Path("subcategoryId") subcategoryId: Int
    ): List<MenuItemResponse>

    @POST("profile")
    suspend fun createUserProfile(
        @Header("Authorization") idToken: String,
        @Body profile: ProfileRequest
    )

    @GET("profile")
    suspend fun getUserProfileByIdToken(
        @Header("Authorization") idToken: String
    ): ProfileResponse

    @GET("user")
    suspend fun isUserRegistered(
        @Header("Authorization") idToken: String
    ): RegisteredResponse

    @GET("cart")
    suspend fun getCart(
        @Header("Authorization") idToken: String
    ): CartResponse

    @FormUrlEncoded
    @POST("cart/item")
    suspend fun addCartItem(
        @Header("Authorization") idToken: String,
        @Field("product_id") productId: String,
        @Field("quantity") quantity: Int = 1,
        @Field("note") note: String = "note"
    ): AddCartItemResponse

    @FormUrlEncoded
    @PATCH("cart/item/{item_id}")
    suspend fun changeCartItemQuantity(
        @Header("Authorization") idToken: String,
        @Path("item_id") itemId: String,
        @Field("quantity") quantity: Int,
        @Field("note") note: String = "note"
    )

    @DELETE("cart/item/{item_id}")
    suspend fun removeCartItem(
        @Header("Authorization") idToken: String,
        @Path("item_id") itemId: String
    )

    @FormUrlEncoded
    @POST("order")
    suspend fun makeOrder(
        @Header("Authorization") idToken: String,
        @Field("table_id") tableId: Int = 1
    ): OrderResponse

    @FormUrlEncoded
    @POST("book")
    suspend fun book(
        @Header("Authorization") idToken: String,
        @Field("order_id") orderId: String,
        @Field("children") children: Int,
        @Field("adults") adult: Int,
        @Field("readyBefore") readyBefore: String,
        @Field("date") data: String
    ): BookResponse

    @FormUrlEncoded
    @POST("payment")
    suspend fun payment(
        @Header("Authorization") idToken: String,
        @Field("order_id") orderId: String,
        @Field("card") card: Long,
        @Field("card_exp_month") cardExpMonth: String,
        @Field("card_exp_year") cardExpYear: String,
        @Field("cvv") cvv: Int
    ): PaymentResponse
}
package com.example.smokestalks.ui.fragment.menu

import com.example.smokestalks.repositories.MenuRepository
import kotlinx.coroutines.*
import moxy.InjectViewState
import moxy.MvpPresenter
import kotlin.coroutines.CoroutineContext

@InjectViewState
class MenuPresenter(
    private val menuRepository: MenuRepository
) : MvpPresenter<MenuView>(), CoroutineScope, CategoryAdapter.CategoryListener,
    SubcategoryAdapter.SubcategoryListener {

    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    private val subcategoryAdapter = SubcategoryAdapter(menuRepository.chosenSubcategoryId)
    private val categoryAdapter = CategoryAdapter(menuRepository.chosenCategoryId)
    private var menuAdapter = MenuAdapter()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        subcategoryAdapter.subcategoryListener = this
        categoryAdapter.categoryListener = this
        viewState.setSubcategoryAdapter(subcategoryAdapter)
        viewState.setCategoryAdapter(categoryAdapter)
        viewState.setMenuAdapter(menuAdapter)

        setCategories()
        setSubcategories()
        setMenu()

    }

    private fun setCategories() {
        launch {
            val categories = menuRepository.getCategories()
            withContext(Dispatchers.Main) {
                categoryAdapter.setCategoryList(categories)
            }
        }

    }

    private fun setSubcategories() {
        launch {
            val subcategories =
                menuRepository.getSubcategoryListByCategoryId(menuRepository.chosenCategoryId)
            withContext(Dispatchers.Main) {
                subcategoryAdapter.setSubcategoryList(subcategories)
            }
        }
    }

    private fun setMenu() {
        if (menuRepository.chosenSubcategoryId != -1) {
            notifySubcategorySelected(menuRepository.chosenSubcategoryId)
        } else {
            notifyCategorySelected(menuRepository.chosenCategoryId)
        }
    }

    override fun notifyCategorySelected(categoryId: Int) {
        menuRepository.chosenCategoryId = categoryId
        menuRepository.chosenSubcategoryId = -1
        launch {
            withContext(Dispatchers.Main) {
                subcategoryAdapter.chosenSubcategoryId = categoryId
                subcategoryAdapter.setSubcategoryList(
                    menuRepository.getSubcategoryListByCategoryId(
                        categoryId
                    )
                )
                menuAdapter.setMenuList(menuRepository.getMenuListByCategoryId(categoryId))
            }
        }
    }

    override fun notifySubcategorySelected(subcategoryId: Int) {
        menuRepository.chosenSubcategoryId = subcategoryId
        launch {
            subcategoryAdapter.chosenSubcategoryId = subcategoryId
            if (subcategoryAdapter.isSubcategoryListEmpty()) {
                val subcategoryList =
                    menuRepository.getSubcategoryListByCategoryId(menuRepository.chosenCategoryId)
                subcategoryAdapter.setSubcategoryList(subcategoryList)
            }
            withContext(Dispatchers.Main) {
                menuAdapter.setMenuList(menuRepository.getMenuListBySubcategoryId(subcategoryId))
            }
        }
    }

    fun requestLogin() {
        viewState.requestLogin()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}


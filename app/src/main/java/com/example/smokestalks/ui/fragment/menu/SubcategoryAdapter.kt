package com.example.smokestalks.ui.fragment.menu

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.smokestalks.R
import com.example.smokestalks.entities.Subcategory
import kotlinx.android.synthetic.main.subcategories_item.view.*
import org.koin.core.KoinComponent

class SubcategoryAdapter(var chosenSubcategoryId: Int) :
    RecyclerView.Adapter<SubcategoryAdapter.SubcategoriesViewHolder>(), KoinComponent {

    lateinit var subcategoryListener: SubcategoryListener
    private val subcategoryList = mutableListOf<Subcategory>()
    private var checkedItem: Subcategory? = null

    fun setSubcategoryList(list: MutableList<Subcategory>) {
        subcategoryList.clear()
        subcategoryList.addAll(list)
        notifyDataSetChanged()
    }

    fun isSubcategoryListEmpty() : Boolean = subcategoryList.isEmpty()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubcategoriesViewHolder {
        return SubcategoriesViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.subcategories_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return subcategoryList.size
    }

    override fun onBindViewHolder(holder: SubcategoriesViewHolder, position: Int) {
        holder.bind(subcategoryList[position], subcategoryListener)
    }


    inner class SubcategoriesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Subcategory, subcategoryListener: SubcategoryListener) {

            if (item.id == chosenSubcategoryId) {
                itemView.subcategoriesTextView.setTextColor(Color.parseColor("#FFCC00"))
                checkedItem = item
            } else {
                itemView.subcategoriesTextView.setTextColor(Color.GRAY)
            }
            itemView.subcategoriesTextView.text = item.name

            itemView.subcategoriesItem.setOnClickListener {
                if (checkedItem != item){
                    itemView.subcategoriesTextView.setTextColor(Color.parseColor("#FFCC00"))
//                    GlobalScope.launch {
//                        presenter.notifySubcategorySelected(item.id)
//                    }
                    if (checkedItem != null)
                        notifyItemChanged(subcategoryList.indexOf(checkedItem!!))
                    checkedItem = item
//                    menuRepository.chosenSubcategoryId = item.id
                    chosenSubcategoryId = item.id
                    subcategoryListener.notifySubcategorySelected(item.id)
                }
            }
        }
    }

    interface SubcategoryListener{
        fun notifySubcategorySelected(subcategoryId: Int)
    }
}
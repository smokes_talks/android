package com.example.smokestalks.ui.fragment.menu

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.smokestalks.R
import com.example.smokestalks.entities.MenuItem
import com.example.smokestalks.repositories.OrderRepository
import kotlinx.android.synthetic.main.menu_item.view.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber

class MenuAdapter : RecyclerView.Adapter<MenuAdapter.MenuItemViewHolder>(), KoinComponent {

    private val menuList = mutableListOf<MenuItem>()
    private val orderRepository: OrderRepository by inject()

    fun setMenuList(list: MutableList<MenuItem>) {
        menuList.clear()
        menuList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: MenuItemViewHolder, position: Int) {
        val menuItem = menuList[position]
        holder.bind(menuItem)

        with(holder.itemView) {
            var count = orderRepository.getCountOfMenuItem(menuItem)
//            Timber.d("Menu item: $menuItem")
//            Timber.d("Count: ${orderRepository.getCountOfMenuItem(menuItem)}")

            if (count != 0) {
                bottomLinearLayout.visibility = View.VISIBLE
                addButton.visibility = View.GONE
                countTextView.text = count.toString() + "x"
            } else {
                countTextView.text = ""
                addButton.visibility = View.VISIBLE
            }

            addButton.setOnClickListener {
//                if (userRepository.isLoginAnonymous()) {
//                    presenter.requestLogin()
//                } else {
                    if (count == 0)
                        count = 1
                    if (count == 1)
                        orderRepository.addCartItem(menuItem)
                    else
                        orderRepository.changeCartItemQuantity(menuItem, count)
                    bottomLinearLayout.visibility = View.VISIBLE
                    countTextView.text = count.toString() + "x"
                addButton.visibility = View.GONE
                //}
            }

            minusButton.setOnClickListener {
                count -= 1
                if (count > 0) {
                    countTextView.text = count.toString() + "x"
                    orderRepository.changeCartItemQuantity(menuItem, count)
                } else {
                    bottomLinearLayout.visibility = View.GONE
                    countTextView.text = ""
                    orderRepository.removeCartItem(menuItem)
                    addButton.visibility = View.VISIBLE
                }
            }

            plusButton.setOnClickListener {
                count += 1
                countTextView.text = count.toString() + "x"
                orderRepository.changeCartItemQuantity(menuItem, count)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuItemViewHolder {
        return MenuItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.menu_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return menuList.size
    }

    class MenuItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bind(menuItem: MenuItem) {
            itemView.dishNameTextView.text = menuItem.title
            itemView.consistTextView.text = menuItem.description
            itemView.priceTextView.text = menuItem.price.toString() + "грн"
            itemView.wightTextView.text = menuItem.size
            itemView.bottomLinearLayout.visibility = View.GONE
        }
    }
}

package com.example.smokestalks.ui.fragment.profile_edit

import com.example.smokestalks.extensions.isValidEmail
import com.example.smokestalks.repositories.UserRepository
import kotlinx.coroutines.*
import moxy.InjectViewState
import moxy.MvpPresenter
import java.util.*
import kotlin.coroutines.CoroutineContext

@InjectViewState
class ProfileEditPresenter(private val userRepository: UserRepository) :
    MvpPresenter<ProfileEditView>(), CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.fillFields(userRepository.currentUser!!)
    }

    fun saveProfile(
        inputName: String,
        inputSurname: String,
        inputEmail: String,
        inputGender: String,
        inputBirthday: Calendar
    ) {
        val userName = inputName.replace(" ", "")
        val userSurname = inputSurname.replace(" ", "")
        val userEmail = inputEmail.replace(" ", "")
        var userGender = inputGender
        if (userGender.isEmpty())
            userGender = "Другой"
        if (userName.isNotEmpty() && userSurname.isNotEmpty() && userEmail.isNotEmpty() && userEmail.isValidEmail()) {
            val date = inputBirthday.time
            userRepository.currentUser!!.apply {
                name = userName
                surname = userSurname
                email = userEmail
                birthday = date
                gender = userGender
                city = "Одесса"
            }
            launch {
                userRepository.updateUserInDatabase(userRepository.currentUser!!)
                userRepository.createUserProfileOnServer(userRepository.currentUser!!)
            }
            viewState.goToProfilePage()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}
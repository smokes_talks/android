package com.example.smokestalks.ui.activity.input_personal_data

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.core.os.bundleOf
import com.example.smokestalks.R
import com.example.smokestalks.ui.activity.MainPageActivity
import kotlinx.android.synthetic.main.personal_data_activity.*
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get
import java.text.SimpleDateFormat
import java.util.*

class InputPersonalDataActivity : MvpAppCompatActivity(), InputPersonalDataView {

    @InjectPresenter
    lateinit var presenter: InputPersonalDataPresenter

    @ProvidePresenter
    fun provideInputPersonalDataPresenter() = get<InputPersonalDataPresenter>()

    private var birthdayDate: Calendar = Calendar.getInstance()
    private val dateFormatter = SimpleDateFormat("d MMM y", Locale("ru"))
//    lateinit var imgr: InputMethodManager
    private val genderPickerBottomSheet = GenderPickerInputPersonalDataActivityBottomSheet()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.personal_data_activity)

        genderTextView.setOnClickListener {
            if (genderTextView.text.isEmpty())
                genderTextView.text = "Другой"
            genderPickerBottomSheet.arguments = bundleOf("gender" to genderTextView.text)
            genderPickerBottomSheet.show(supportFragmentManager, "genderPicker")
        }

        birthdayTextView.setOnClickListener {
            val datePickerDialog = DatePickerDialog(
                this, onDateListener,
                birthdayDate[Calendar.YEAR],
                birthdayDate[Calendar.MONTH],
                birthdayDate[Calendar.DAY_OF_MONTH]
            )
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() + 10
            datePickerDialog.show()
        }

        saveProfileDataButton.setOnClickListener {
            presenter.saveProfile(nameEditText.text.toString(), surnameEditText.text.toString(), emailEditText.text.toString(),
            genderTextView.text.toString(), birthdayDate)
        }

        skipTextView.setOnClickListener {
            val intent = Intent(this, MainPageActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    // установка обработчика выбора даты
    private var onDateListener =
        OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            birthdayDate[Calendar.YEAR] = year
            birthdayDate[Calendar.MONTH] = monthOfYear
            birthdayDate[Calendar.DAY_OF_MONTH] = dayOfMonth
            birthdayDateTextView.text = "${dateFormatter.format(birthdayDate.time)} г."
        }

    fun setSelectedGender(gender: String) {
        genderTextView.text = gender
        genderTextView.setTextColor(Color.parseColor("#ffffff"))
    }

    override fun setInitialDate(){
        birthdayDate.time = Date()
        birthdayDate[Calendar.YEAR] = 2000
        birthdayDate[Calendar.MONTH] = 0
        birthdayDate[Calendar.DAY_OF_MONTH] = 1

        birthdayDateTextView.text = "${dateFormatter.format(birthdayDate.time)} г."
    }

//    override fun openKeyboard() {
//        nameEditText.requestFocus()
//        imgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
//    }

    override fun goToMainPageActivity() {
        val intent = Intent(this, MainPageActivity::class.java)
        startActivity(intent)
    }

    override fun finishActivity() {
        finish()
    }
}
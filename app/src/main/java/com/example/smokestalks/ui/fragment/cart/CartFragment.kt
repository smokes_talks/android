package com.example.smokestalks.ui.fragment.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smokestalks.R
import com.example.smokestalks.ui.FragmentsRouters
import com.example.smokestalks.ui.Visibility
import com.example.smokestalks.ui.fragment.reservation.ReservationFragment
import com.example.smokestalks.repositories.OrderRepository
import kotlinx.android.synthetic.main.cart_fragment.*
import kotlinx.android.synthetic.main.main_activity.*
import org.koin.android.ext.android.inject

class CartFragment : Fragment() {

    private val orderRepository: OrderRepository by inject()
    private val dishOrderAdapter = OrderAdapter(orderRepository.getCartItems())
    private lateinit var sumOrderObserver: Observer<Int>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.cart_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val hide = activity as? Visibility
        hide?.hideView()

        activity?.bottomNavigationView?.visibility = View.GONE
        activity?.titleToolbar?.visibility = View.GONE

        val router = activity as? FragmentsRouters

        cartPageBackButton.setOnClickListener {
            activity?.onBackPressed()
        }

        sumOrderObserver = Observer<Int>{ value ->
            sumOrderCartPageTextView.text = value.toString()
        }

        orderRecyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = dishOrderAdapter
            addItemDecoration(
                DividerItemDecoration(
                    view.context,
                    LinearLayoutManager.VERTICAL
                )
            )
        }

        createOrderButton.setOnClickListener {
            router?.navigateToFragment(ReservationFragment(), null)
        }
    }

    override fun onStart() {
        super.onStart()
        orderRepository.sumOrder.observe(requireActivity(), sumOrderObserver)
    }

    override fun onPause() {
        super.onPause()
        orderRepository.sumOrder.removeObserver(sumOrderObserver)
    }
}

package com.example.smokestalks.ui.activity.input_personal_data

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle

@AddToEndSingle
interface InputPersonalDataView: MvpView {
    fun setInitialDate()
//    fun openKeyboard()
    fun goToMainPageActivity()
    fun finishActivity()
}
package com.example.smokestalks.ui.dialogs

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.smokestalks.R
import com.example.smokestalks.ui.activity.AuthorizationActivity
import com.example.smokestalks.repositories.MenuRepository
import com.example.smokestalks.repositories.OrderRepository
import com.example.smokestalks.repositories.UserRepository
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.exit_dialog_fragment.*
import org.koin.android.ext.android.inject

class ExitDialogFragment : DialogFragment() {

    private val userRepository: UserRepository by inject()
    private val mAuth = FirebaseAuth.getInstance()
    private val orderRepository: OrderRepository by inject()
    private val menuRepository: MenuRepository by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.exit_dialog_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        exitButton.setOnClickListener {
            mAuth.signOut()
            userRepository.clearAuthorizedUserIdToken()
            userRepository.currentUser = null
            userRepository.setAnonymousLoginToFalse()
            orderRepository.clearCart()
            menuRepository.chosenCategoryId = 2
            menuRepository.chosenSubcategoryId = -1
            val intent = Intent(activity, AuthorizationActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }

        cancelButton.setOnClickListener {
            dismiss()
        }
    }
}
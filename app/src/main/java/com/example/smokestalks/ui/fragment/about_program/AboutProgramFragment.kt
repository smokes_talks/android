package com.example.smokestalks.ui.fragment.about_program

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.smokestalks.R
import com.example.smokestalks.ui.Visibility
import kotlinx.android.synthetic.main.about_program_fragment.*
import kotlinx.android.synthetic.main.actionbar.view.*
import kotlinx.android.synthetic.main.main_activity.*

class AboutProgramFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.about_program_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.bottomNavigationView?.visibility = View.GONE

        toolbar.backButton.setOnClickListener {
            requireActivity().onBackPressed()
        }

        val hide = activity as? Visibility
        hide?.hideView()
    }
}
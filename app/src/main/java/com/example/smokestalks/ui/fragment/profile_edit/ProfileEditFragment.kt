package com.example.smokestalks.ui.fragment.profile_edit

import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.os.bundleOf
import com.example.smokestalks.R
import com.example.smokestalks.entities.User
import com.example.smokestalks.extensions.formatPhoneText
import com.example.smokestalks.ui.Visibility
import kotlinx.android.synthetic.main.actionbar.view.*
import kotlinx.android.synthetic.main.editing_profile_fragment.*
import kotlinx.android.synthetic.main.editing_profile_fragment.birthdayDateTextView
import kotlinx.android.synthetic.main.editing_profile_fragment.birthdayTextView
import kotlinx.android.synthetic.main.editing_profile_fragment.emailEditText
import kotlinx.android.synthetic.main.editing_profile_fragment.genderTextView
import kotlinx.android.synthetic.main.editing_profile_fragment.nameEditText
import kotlinx.android.synthetic.main.editing_profile_fragment.surnameEditText
import kotlinx.android.synthetic.main.main_activity.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class ProfileEditFragment : MvpAppCompatFragment(), ProfileEditView {

    @InjectPresenter
    lateinit var presenter: ProfileEditPresenter

    @ProvidePresenter
    fun provideProfileEditPresenter() = get<ProfileEditPresenter>()

    private val genderPickerBottomSheet = GenderPickerEditPersonalDataBottomSheet()
    private var birthdayDate: Calendar = Calendar.getInstance()
    private val dateFormatter = SimpleDateFormat("d MMM y", Locale("ru"))
    lateinit var imgr: InputMethodManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.editing_profile_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requireActivity().bottomNavigationView.visibility = View.GONE
        val hide = activity as? Visibility
        hide?.hideView()

        toolbar.backButton.setOnClickListener {
            requireActivity().onBackPressed()
        }

        genderTextView.setOnClickListener {
            if (genderTextView.text.isEmpty())
                genderTextView.text = "Другой"
            genderPickerBottomSheet.arguments = bundleOf("gender" to genderTextView.text)
            genderPickerBottomSheet.show(requireActivity().supportFragmentManager, "genderPicker")
        }

        birthdayTextView.setOnClickListener {
            val datePickerDialog = DatePickerDialog(
                requireContext(), onDateListener,
                birthdayDate[Calendar.YEAR],
                birthdayDate[Calendar.MONTH],
                birthdayDate[Calendar.DAY_OF_MONTH]
            )
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() + 10
            datePickerDialog.show()
        }

        saveEditedProfileDataButton.setOnClickListener {
            presenter.saveProfile(
                nameEditText.text.toString(),
                surnameEditText.text.toString(),
                emailEditText.text.toString(),
                genderTextView.text.toString(),
                this.birthdayDate
            )
        }
    }

    override fun goToProfilePage() {
        activity?.onBackPressed()
    }

    override fun fillFields(authUser: User) {
        Timber.d("Current user: $authUser")
        nameEditText.setText(authUser.name)
        surnameEditText.setText(authUser.surname)
        emailEditText.setText(authUser.email)
        if (authUser.birthday == null) {
            birthdayDate.time = Date()
            birthdayDate[Calendar.YEAR] = 2000
            birthdayDate[Calendar.MONTH] = 0
            birthdayDate[Calendar.DAY_OF_MONTH] = 1
        } else
            this.birthdayDate = dateToCalendar(authUser.birthday!!)
        if (authUser.gender!!.isNotEmpty()) {
            genderTextView.text = authUser.gender
            genderTextView.setTextColor(Color.parseColor("#ffffff"))
        }

        birthdayDateTextView.text = "${dateFormatter.format(birthdayDate.time)} г."
        //phoneNumberTextView.text = userRepository.currentUser!!.phone.formatPhoneText()
        phoneNumberTextView.text = authUser.phone.formatPhoneText()
    }

    // установка обработчика выбора даты
    private var onDateListener =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            this.birthdayDate[Calendar.YEAR] = year
            this.birthdayDate[Calendar.MONTH] = monthOfYear
            this.birthdayDate[Calendar.DAY_OF_MONTH] = dayOfMonth
            birthdayDateTextView.text = "${dateFormatter.format(birthdayDate.time)} г."
        }

//    private fun getIndexOfSpinnerByValue(spinner: Spinner, myString: String): Int {
//        for (i in 0 until spinner.count) {
//            Timber.d(spinner.getItemAtPosition(i).toString())
//            if (spinner.getItemAtPosition(i).toString().equals(myString, ignoreCase = true)) {
//                return i
//            }
//        }
//        return 0
//    }

    private fun dateToCalendar(date: Date): Calendar {
        val calendar = Calendar.getInstance()
        calendar.time = date
        return calendar
    }

    override fun onDestroyView() {
        super.onDestroyView()
        requireActivity().bottomNavigationView.visibility = View.VISIBLE
    }

    override fun onStop() {
        super.onStop()
        imgr =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imgr.hideSoftInputFromWindow(view?.rootView?.windowToken, 0)
    }
}
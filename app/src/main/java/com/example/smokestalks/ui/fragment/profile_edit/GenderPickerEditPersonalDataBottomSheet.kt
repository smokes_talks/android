package com.example.smokestalks.ui.fragment.profile_edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.smokestalks.R
import com.example.smokestalks.ui.activity.MainPageActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.gender_picker.*
import timber.log.Timber

class GenderPickerEditPersonalDataBottomSheet : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.gender_picker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val genderArray = resources.getStringArray(R.array.gender_array)
        Timber.d("Gender: ${arguments?.getString("gender")}")
        genderPicker.apply {
            displayedValues = genderArray
            value = genderArray.indexOf(arguments?.getString("gender")) + 1
        }

        // OnScrollListener
        genderPicker.setOnValueChangedListener { _, _, newVal ->
            (activity as MainPageActivity).setSelectedGender(genderArray[newVal - 1])
            Timber.d("newVal: ${genderArray[newVal - 1]}")
        }
    }
}
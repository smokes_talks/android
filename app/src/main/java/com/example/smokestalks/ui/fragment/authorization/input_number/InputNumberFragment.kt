package com.example.smokestalks.ui.fragment.authorization.input_number

import android.content.Context
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.graphics.Shader.TileMode
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.smokestalks.Constants
import com.example.smokestalks.R
import com.example.smokestalks.ui.fragment.authorization.input_code.InputCodeFragment
import com.example.smokestalks.ui.activity.AuthorizationActivity
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.actionbar.view.*
import kotlinx.android.synthetic.main.input_number_fragment.*
import java.util.concurrent.TimeUnit


class InputNumberFragment : Fragment() {

    private lateinit var imgr: InputMethodManager
    lateinit var phoneNumber: String
    private val inputCodeFragment: InputCodeFragment? = null
    private var mVerificationId: String? = null
    private val mAuth = Firebase.auth

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.input_number_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        openKeyboard()
        setGradientForErrorText()

        action_bar.backButton.setOnClickListener {
            findNavController().navigate(R.id.action_inputNumberFragment_to_startPageFragment)
        }

        loginButton.setOnClickListener {
//            if (phoneNumber.length == 17){
            phoneNumber = numberEditText.text.toString()
            (activity as AuthorizationActivity).isStoragePermissionGranted()
            sendVerificationCode(phoneNumber)
//            }
//            phoneNumber = numberEditText.text?.replace((" ").toRegex(), "")!!
//            if (phoneNumber.length < 13)
//                errorTextView.visibility = View.VISIBLE
//            else
//                sendVerificationCode(phoneNumber)

        }

        numberEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                errorTextView.visibility = View.INVISIBLE
                if (count == 17) {
                    rippleLayout.visibility = View.VISIBLE
                    grey_out_view.visibility = View.GONE
                } else {
                    rippleLayout.visibility = View.GONE
                    grey_out_view.visibility = View.VISIBLE
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })
    }

    private fun sendVerificationCode(phoneNumber: String) {
        val options = PhoneAuthOptions.newBuilder(mAuth)
            .setPhoneNumber(phoneNumber) // Phone number to verify
            .setTimeout(10L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(requireActivity()) // Activity (for callback binding)
            .setCallbacks(mCallBack) // OnVerificationStateChangedCallbacks
            .build()
        verifyPhoneNumber(options)
    }

    private val mCallBack: OnVerificationStateChangedCallbacks =
        object : OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                val code = phoneAuthCredential.smsCode
                if (code != null) {
                    inputCodeFragment?.verifyVerificationCode(code)
                } else {
                    findNavController().navigate(R.id.action_inputNumberFragment_to_menuActivity)
                }
            }

            override fun onVerificationFailed(e: FirebaseException) {
                if (haveNetworkConnection()) {
                    Toast.makeText(
                        context, "Something went wrong, try again later please",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    errorTextView.visibility = View.VISIBLE
                }
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                mVerificationId = verificationId
                val bundle = Bundle()
                bundle.putString(Constants.SEND_ID, mVerificationId)
                bundle.putString("phone", phoneNumber)
                findNavController().navigate(
                    R.id.action_inputNumberFragment_to_inputCodeFragment,
                    bundle
                )
            }
        }


    override fun onPause() {
        super.onPause()
        // закрывает клавиатуру
        imgr.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    private fun haveNetworkConnection(): Boolean {
        var haveConnectedWifi = false
        var haveConnectedMobile = false
        val cm =
            requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val netInfo = cm!!.allNetworkInfo
        for (ni in netInfo) {
            if (ni.typeName.equals(
                    "WIFI",
                    ignoreCase = true
                )
            ) if (ni.isConnected) haveConnectedWifi = true
            if (ni.typeName.equals(
                    "MOBILE",
                    ignoreCase = true
                )
            ) if (ni.isConnected) haveConnectedMobile = true
        }
        return haveConnectedWifi || haveConnectedMobile
    }

    private fun openKeyboard(){
        // открывает клавиатуру
        numberEditText.requestFocus()
        imgr =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    private fun setGradientForErrorText(){
        val paint = errorTextView.paint
        val width = paint.measureText(errorTextView.text.toString())
        val textShader: Shader = LinearGradient(
            0f, 0f, width, errorTextView.textSize, intArrayOf(
                Color.parseColor("#F56C46"),
                Color.parseColor("#EEA749")
            ), null, TileMode.REPEAT
        )
        errorTextView.paint.shader = textShader
    }

}
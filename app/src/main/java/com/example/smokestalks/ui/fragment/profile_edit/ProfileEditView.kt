package com.example.smokestalks.ui.fragment.profile_edit

import com.example.smokestalks.entities.User
import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle

@AddToEndSingle
interface ProfileEditView: MvpView {
    fun fillFields(authUser: User)
    fun goToProfilePage()
}
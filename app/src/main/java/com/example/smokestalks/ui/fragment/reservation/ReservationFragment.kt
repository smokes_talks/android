package com.example.smokestalks.ui.fragment.reservation

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.example.smokestalks.R
import com.example.smokestalks.ui.FragmentsRouters
import com.example.smokestalks.ui.fragment.payment.PaymentFragment
import kotlinx.android.synthetic.main.fragment_reservation.*
import java.text.SimpleDateFormat
import java.util.*

class ReservationFragment : Fragment(){

//    private val qrCodeScanner = QRCodeBottomSheetFragment()
    private val dateFormatter = SimpleDateFormat("d MMM", Locale("ru"))
    private var adultsNumber = 0
    private var childrenNumber = 0

    private val reservationDate = Calendar.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reservation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val router = activity as? FragmentsRouters

        setDate(reservationDate)

        adultsPlusButton.setOnClickListener {
            adultsNumber += 1
            adultsNumberTextView.text = adultsNumber.toString()
        }

        adultsMinusButton.setOnClickListener {
            if (adultsNumber > 0){
                adultsNumber -= 1
                adultsNumberTextView.text = adultsNumber.toString()
            }
        }

        childrenPlusButton.setOnClickListener {
            childrenNumber += 1
            childrenNumberTextView.text = childrenNumber.toString()
        }

        childrenMinusButton.setOnClickListener {
            if (childrenNumber > 0){
                childrenNumber -= 1
                childrenNumberTextView.text = childrenNumber.toString()
            }
        }

        orderDateTextView.setOnClickListener {
            val d = OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                reservationDate[Calendar.YEAR] = year
                reservationDate[Calendar.MONTH] = monthOfYear
                reservationDate[Calendar.DAY_OF_MONTH] = dayOfMonth
                orderDateTextView.text = dateFormatter.format(reservationDate.time)
            }
            val datePickerDialog = DatePickerDialog(
                requireContext(), d,
                reservationDate[Calendar.YEAR],
                reservationDate[Calendar.MONTH],
                reservationDate[Calendar.DAY_OF_MONTH]
            )
            datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 10
            datePickerDialog.show()
        }

        createOrderButton.setOnClickListener {
//            if (adultsNumber == 0)
//                Toast.makeText(activity, "Adults number cannot be 0", Toast.LENGTH_LONG).show()
//            else {
//                qrCodeScanner.show(requireActivity().supportFragmentManager, "QRCode")
//                // replace to QR code page later
//                makeOrder()
//            }

            val timeFormat = if (timeFormatSegmentedButtonGroup.position == 0)
                Calendar.AM
            else
                Calendar.PM

            val bundle = bundleOf(
                "adultsNumber" to adultsNumber,
                "childrenNumber" to childrenNumber,
                "reservationDate" to reservationDate,
                "hour" to hoursNumberPicker.value,
                "minute" to minutesNumberPicker.value,
                "timeFormat" to timeFormat
            )
            router?.navigateToFragment(PaymentFragment(), bundle)

//            presenter.makeOrder(hoursNumberPicker.value, minutesNumberPicker.value, timeFormat)
        }

        reservationPageBackButton.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    private fun setDate(date: Calendar) {
        date.time = Date()

        orderDateTextView.text = dateFormatter.format(date.time)
        hoursNumberPicker.apply {
            value = date[Calendar.HOUR]
            displayedValues = resources.getStringArray(R.array.hour_array)
        }
        minutesNumberPicker.apply {
            value = date[Calendar.MINUTE]
            displayedValues = resources.getStringArray(R.array.minute_array)
        }

        if (date[Calendar.AM_PM] == Calendar.PM)
            timeFormatSegmentedButtonGroup.setPosition(1, false)
    }

//    override fun showQRCode() {
//        qrCodeScanner.show(requireActivity().supportFragmentManager, "QRCode")
//    }

}

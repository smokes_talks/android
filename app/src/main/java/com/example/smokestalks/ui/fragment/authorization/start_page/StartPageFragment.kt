package com.example.smokestalks.ui.fragment.authorization.start_page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.smokestalks.R
import com.example.smokestalks.repositories.MenuRepository
import com.example.smokestalks.repositories.UserRepository
import kotlinx.android.synthetic.main.start_page_fragment.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.core.KoinComponent

class StartPageFragment : Fragment(), KoinComponent{

    private val userRepository: UserRepository by inject()
    private val menuRepository: MenuRepository by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.start_page_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        numberButton.setOnClickListener {
            findNavController().navigate(R.id.action_startPageFragment_to_inputNumberFragment)
        }

        if (userRepository.getAuthorizedUserIdToken()
                .isNotEmpty() || userRepository.isLoginAnonymous()
        ) {
//            findNavController().navigate(R.id.menuActivity)
            findNavController().navigate(R.id.action_startPageFragment_to_loadingActivity)
            activity?.finish()
        } else {
            GlobalScope.launch {
                // списки категорий и меню будут храниться в качестве параметров в репозитории
                menuRepository.loadCategories()
                menuRepository.loadMenu()
            }
        }

//        loginWithoutAuthButton.setOnClickListener {
//            userRepository.setAnonymousLogin()
//            findNavController().navigate(R.id.menuActivity)
//            activity?.finish()
//        }
    }
}
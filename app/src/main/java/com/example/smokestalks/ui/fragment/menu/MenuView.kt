package com.example.smokestalks.ui.fragment.menu

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle

@AddToEndSingle
interface MenuView : MvpView {
    fun setCategoryAdapter(categoryAdapter: CategoryAdapter)
    fun setSubcategoryAdapter(subcategoryAdapter: SubcategoryAdapter)
    fun setMenuAdapter(menuAdapter: MenuAdapter)
    fun requestLogin()
}
package com.example.smokestalks.ui.fragment.authorization.input_code

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle

@AddToEndSingle
interface InputCodeView: MvpView {
    fun goToInputPersonalDataActivity()
    fun goToMenuActivity()
    fun finishActivity()
    fun wrongInputCode()
}
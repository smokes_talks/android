package com.example.smokestalks.ui.fragment.cart

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.smokestalks.R
import com.example.smokestalks.entities.MenuItem
import com.example.smokestalks.repositories.OrderRepository
import kotlinx.android.synthetic.main.menu_item.view.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber

class OrderAdapter(
    private val menuList: MutableList<MenuItem>
) :
    RecyclerView.Adapter<OrderAdapter.OrderViewHolder>(), KoinComponent {

    private val orderRepository: OrderRepository by inject()

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        val menuItem = menuList[position]
        holder.bind(menuItem)

        with(holder.view){
            var count = orderRepository.getCountOfMenuItem(menuItem)
            countTextView.text = count.toString() + "x"

            minusButton.setOnClickListener {
                count -= 1
                if (count > 0) {
                    countTextView.text = count.toString() + "x"
                    orderRepository.changeCartItemQuantity(menuItem, count)
//                    fragment.setSumOrder(orderRepository.sumOrder)
                } else {
                    val dishPosition = menuList.indexOf(menuItem)
                    menuList.removeAt(dishPosition)
                    notifyItemRemoved(dishPosition)
                    orderRepository.removeCartItem(menuItem)
//                    fragment.setSumOrder(orderRepository.sumOrder)
                }
            }

            plusButton.setOnClickListener {
                count += 1
                countTextView.text = count.toString() + "x"
                orderRepository.changeCartItemQuantity(menuItem, count)
//                fragment.setSumOrder(orderRepository.sumOrder)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        return OrderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.menu_order_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return menuList.size
    }

    class OrderViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(menuItem: MenuItem) {
            itemView.dishNameTextView.text = menuItem.title
            itemView.consistTextView.text = menuItem.description
            itemView.priceTextView.text = menuItem.price.toString() + "грн"
            itemView.wightTextView.text = menuItem.size
        }
    }
}

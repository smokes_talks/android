package com.example.smokestalks.ui.fragment.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smokestalks.R
import com.example.smokestalks.entities.News
import com.example.smokestalks.ui.activity.MainPageActivity
import kotlinx.android.synthetic.main.main_activity.*
import kotlinx.android.synthetic.main.news_fragment.*

class NewsFragment: Fragment() {

    private val newsList = listOf(
        News("По понедельникам у нас играет джаз и второй коктейль в подарок \uD83D\uDE0E", R.drawable.test_photo),
        News("По понедельникам у нас играет джаз и второй коктейль в подарок \uD83D\uDE0E", R.drawable.test_photo),
        News("По понедельникам у нас играет джаз и второй коктейль в подарок \uD83D\uDE0E", R.drawable.test_photo),
        News("По понедельникам у нас играет джаз и второй коктейль в подарок \uD83D\uDE0E", R.drawable.test_photo),
        News("По понедельникам у нас играет джаз и второй коктейль в подарок \uD83D\uDE0E", R.drawable.test_photo)
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.news_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.bottomNavigationView?.visibility = View.VISIBLE
        activity?.titleToolbar?.visibility = View.VISIBLE

        newsRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = NewsAdapter(newsList)
        }
    }

}
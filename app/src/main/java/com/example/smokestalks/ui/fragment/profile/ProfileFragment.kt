package com.example.smokestalks.ui.fragment.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.smokestalks.R
import com.example.smokestalks.ui.fragment.about_program.AboutProgramFragment
import com.example.smokestalks.ui.fragment.order_history.OrderHistoryFragment
import com.example.smokestalks.ui.FragmentsRouters
import com.example.smokestalks.ui.activity.MainPageActivity
import com.example.smokestalks.ui.fragment.profile_edit.ProfileEditFragment
import com.example.smokestalks.ui.dialogs.ExitDialogFragment
import com.example.smokestalks.ui.dialogs.LoginDialogFragment
import com.example.smokestalks.repositories.UserRepository
import kotlinx.android.synthetic.main.main_activity.*
import kotlinx.android.synthetic.main.profile_fragment.*
import org.koin.android.ext.android.inject

class ProfileFragment : Fragment() {

    private val userRepository: UserRepository by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.bottomNavigationView?.visibility = View.VISIBLE
        activity?.titleToolbar?.visibility = View.VISIBLE

        if (userRepository.currentUser?.name.isNullOrEmpty()) {
            userNameTextView.text = "Тут будет ваше имя"
        } else {
            userNameTextView.text =
                userRepository.currentUser!!.name + " " + userRepository.currentUser!!.surname
        }

        val router = activity as? FragmentsRouters

        editProfileLayout.setOnClickListener {
            if (userRepository.isLoginAnonymous())
                requestLogin()
            else
                router?.navigateToFragment(ProfileEditFragment(), null)
        }
        aboutProgramLayout.setOnClickListener {
            router?.navigateToFragment(AboutProgramFragment(), null)
        }
        orderHistoryLayout.setOnClickListener {
            if (userRepository.isLoginAnonymous())
                requestLogin()
            else
                router?.navigateToFragment(OrderHistoryFragment(), null)
        }

        exitLayout.setOnClickListener {
            val fragmentTransaction = parentFragmentManager.beginTransaction()
            fragmentTransaction.addToBackStack(null)
            val dialogFragment = ExitDialogFragment()
            dialogFragment.show(fragmentTransaction, "dialog")
        }
    }


    private fun requestLogin() {
        val fragmentTransaction = parentFragmentManager.beginTransaction()
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = LoginDialogFragment()
        dialogFragment.show(fragmentTransaction, "dialog")
    }
}
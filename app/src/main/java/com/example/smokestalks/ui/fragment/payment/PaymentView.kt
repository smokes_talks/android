package com.example.smokestalks.ui.fragment.payment

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle

@AddToEndSingle
interface PaymentView : MvpView {
    fun goToMainPageActivity()
}
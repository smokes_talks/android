package com.example.smokestalks.ui.fragment.bonus

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.smokestalks.R
import com.example.smokestalks.ui.activity.MainPageActivity
import kotlinx.android.synthetic.main.bonuses_fragment.*
import kotlinx.android.synthetic.main.main_activity.*

class BonusFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bonuses_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.bottomNavigationView?.visibility = View.VISIBLE
        activity?.titleToolbar?.visibility = View.VISIBLE

        imageView3.setOnClickListener {
            throw Exception("Test Exception")
        }

    }

}
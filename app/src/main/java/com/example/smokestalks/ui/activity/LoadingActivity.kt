package com.example.smokestalks.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.smokestalks.R
import com.example.smokestalks.repositories.MenuRepository
import com.example.smokestalks.repositories.OrderRepository
import com.example.smokestalks.repositories.UserRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class LoadingActivity : AppCompatActivity() {

    private val menuRepository: MenuRepository by inject()
    private val userRepository: UserRepository by inject()
    private val orderRepository: OrderRepository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)

        GlobalScope.launch {
            menuRepository.loadCategories()
            menuRepository.loadMenu()
            if (userRepository.getAuthorizedUserIdToken().isNotEmpty())
                orderRepository.loadCart()
            startActivity(Intent(this@LoadingActivity, MainPageActivity::class.java))
        }
    }
}
package com.example.smokestalks.ui.activity

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.example.smokestalks.R
import com.example.smokestalks.ui.FragmentsRouters
import com.example.smokestalks.ui.Visibility
import com.example.smokestalks.ui.fragment.bonus.BonusFragment
import com.example.smokestalks.ui.fragment.menu.MenuFragment
import com.example.smokestalks.ui.fragment.news.NewsFragment
import com.example.smokestalks.ui.fragment.profile.ProfileFragment
import com.example.smokestalks.ui.dialogs.LoginDialogFragment
import com.example.smokestalks.ui.fragment.cart.CartFragment
import com.example.smokestalks.repositories.OrderRepository
import com.example.smokestalks.repositories.UserRepository
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.editing_profile_fragment.*
import kotlinx.android.synthetic.main.main_activity.*
import kotlinx.android.synthetic.main.main_activity.view.*
import kotlinx.coroutines.*
import org.koin.android.ext.android.inject
import timber.log.Timber
import java.util.*

class MainPageActivity : AppCompatActivity(),
    FragmentsRouters, Visibility {

    private val userRepository: UserRepository by inject()
    private val orderRepository: OrderRepository by inject()
    private lateinit var sumOrderObserver: Observer<Int>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

//        titleToolbar.sumOrderTextView.text = orderRepository.sumOrder.value.toString() + " грн"
        sumOrderObserver = Observer<Int>{ value ->
            titleToolbar.sumOrderTextView.text = value.toString() + " грн"
        }

        Timber.d("Token: ${userRepository.getAuthorizedUserIdToken()}")

        if (!userRepository.isLoginAnonymous()) {
            GlobalScope.launch {
                userRepository.loadCurrentUser()
                orderRepository.loadCart()
                withContext(Dispatchers.Main) {
//                    updateOrderSum()
                }
            }
        }

        setSupportActionBar(titleToolbar)

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNavigationView.itemBackground
        val fragment = MenuFragment()
        addFragment(fragment)
        orderField.setOnClickListener {
            when {
                userRepository.isLoginAnonymous() -> {
                    val dialogFragment = LoginDialogFragment()
                    dialogFragment.show(supportFragmentManager, "dialog")
                }
                orderRepository.isCartEmpty() -> Toast.makeText(
                    this,
                    "Корзина пуста",
                    Toast.LENGTH_LONG
                ).show()
                else -> navigateToFragment(CartFragment(), null)
            }
        }
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            if (item.itemId == bottomNavigationView.selectedItemId)
                return@OnNavigationItemSelectedListener true
            when (item.itemId) {
                R.id.navigation_menu -> {
                    val fragment =
                        MenuFragment()
                    titleTextView.text = "Меню"
                    titleToolbar.visibility = View.VISIBLE
                    addFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_news -> {
                    val fragment =
                        NewsFragment()
                    titleTextView.text = "Новости"
                    titleToolbar.visibility = View.VISIBLE
                    addFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_bonus -> {
                    val fragment = BonusFragment()
                    titleTextView.text = "Бонусы"
                    titleToolbar.visibility = View.VISIBLE
                    addFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_account -> {
                    val fragment = ProfileFragment()
                    titleTextView.text = "Профиль"
                    titleToolbar.visibility = View.VISIBLE
                    addFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.fragment_fade_enter,
                R.anim.fragment_close_exit
            )
            .replace(R.id.container, fragment, fragment.javaClass.simpleName)
            .commit()
    }

    override fun navigateToFragment(fragment: Fragment, bundle: Bundle?) {
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .setCustomAnimations(
                R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }

    override fun hideView() {
        titleToolbar.visibility = View.GONE
    }

    override fun showView() {
        orderField.visibility = View.VISIBLE
    }

    fun setSelectedGender(gender: String) {
        genderTextView.text = gender
        genderTextView.setTextColor(Color.parseColor("#ffffff"))
    }

    override fun onStart() {
        super.onStart()
        orderRepository.sumOrder.observe(this, sumOrderObserver)
    }

    override fun onPause() {
        super.onPause()
        orderRepository.sumOrder.removeObserver (sumOrderObserver)
    }
}
package com.example.smokestalks.ui.fragment.authorization.input_code

import com.example.smokestalks.entities.User
import com.example.smokestalks.repositories.OrderRepository
import com.example.smokestalks.repositories.UserRepository
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.*
import moxy.InjectViewState
import moxy.MvpPresenter
import timber.log.Timber
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.coroutines.CoroutineContext

@InjectViewState
class InputCodePresenter(
    private val userRepository: UserRepository,
    private val orderRepository: OrderRepository
): MvpPresenter<InputCodeView>(), CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    val mAuth = Firebase.auth

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        mAuth.setLanguageCode(Locale.getDefault().language)
    }

    fun parseCode(message: String?): String {
        val p: Pattern = Pattern.compile("\\b\\d{6}\\b")
        val m: Matcher = p.matcher(message.toString())
        var code = ""
        while (m.find()) {
            code = m.group(0)
        }
        return code
    }

    fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential, phoneNumber: String) {
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    mAuth.currentUser!!.getIdToken(true)
                        .addOnSuccessListener { tokenResult ->
                            val idToken = tokenResult.token!!
                            Timber.d("Token: $idToken")
                            launch {
                                userRepository.setAuthorizedUserIdToken(idToken)
                                orderRepository.loadCart()
                                // получаем пользователя из базы данных по uid
                                var user = userRepository.getUserByUid(mAuth.currentUser!!.uid)
                                // если такой пользователь есть в базе данных
                                if (user != null) {
                                    // если пользователь авторизировался, но не указал информацию о себе
                                    userRepository.currentUser = user
                                    if (user.name.isNullOrEmpty()) {
                                        viewState.goToInputPersonalDataActivity()
                                    }
                                    // если авторизировался и указал информацию о себе переходим на главную страницу
                                    else {
                                        viewState.goToMenuActivity()
                                        viewState.finishActivity()
                                        userRepository.currentUser = user
                                    }
                                } else {
                                    // проверяем если ли такой в базе данных на сервере
                                    val isRegistered = userRepository.isCurrentUserRegistered()
                                    if (isRegistered) {
                                        user = userRepository.getCurrentUserFromServer()
                                        userRepository.insertUserToDatabase(user)
                                        userRepository.currentUser = user
                                        // если пользователь авторизировался, но не указал информацию о себе
                                        if (user.name.isNullOrEmpty()) {
                                            viewState.goToInputPersonalDataActivity()
                                        }
                                        // если авторизировался и указал информацию о себе переходим на главную страницу
                                        else {
                                            viewState.goToMenuActivity()
                                            viewState.finishActivity()
                                        }
                                    } else {
                                        val newUser = User(
                                            mAuth.currentUser!!.uid,
                                            phoneNumber,
                                            "",
                                            "",
                                            "",
                                            null,
                                            "Другой",
                                            "Одесса"
                                        )
                                        userRepository.createUserProfileOnServer(newUser)
                                        userRepository.insertUserToDatabase(newUser)
                                        userRepository.currentUser = newUser
                                        viewState.goToInputPersonalDataActivity()
                                        viewState.finishActivity()
                                    }
                                }
                            }
                        }

                } else {
                    viewState.wrongInputCode()
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}
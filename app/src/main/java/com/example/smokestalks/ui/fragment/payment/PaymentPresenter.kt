package com.example.smokestalks.ui.fragment.payment

import com.example.smokestalks.api.APIService
import com.example.smokestalks.repositories.OrderRepository
import com.example.smokestalks.repositories.UserRepository
import kotlinx.coroutines.*
import moxy.InjectViewState
import moxy.MvpPresenter
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

@InjectViewState
class PaymentPresenter(
    private val apiService: APIService,
    private val userRepository: UserRepository,
    private val orderRepository: OrderRepository,
) : MvpPresenter<PaymentView>(), CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    fun makeOrder(
        adultsNumber: Int,
        childrenNumber: Int,
        reservationDate: Calendar,
        hour: Int,
        minute: Int,
        timeFormat: Int,
        card: Long,
        cardExpMonth: String,
        cardExpYear: String,
        cvv: Int
    ) {
        reservationDate[Calendar.HOUR] = hour
        reservationDate[Calendar.MINUTE] = minute
        reservationDate[Calendar.AM_PM] = timeFormat
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ", Locale.US)

        val dateString = formatter.format(reservationDate.time)

        val readyBefore = Calendar.getInstance()
        readyBefore.time = reservationDate.time
        readyBefore.add(Calendar.MINUTE, -10)
        val readyBeforeString = formatter.format(readyBefore.time)

        launch {
            val orderResponse = try {
                apiService.makeOrder(userRepository.getAuthorizedUserIdToken())
            } catch (e: retrofit2.HttpException) {
                userRepository.updateIdToken()
                apiService.makeOrder(userRepository.getAuthorizedUserIdToken())
            }
            Timber.d("ReadyBefore: $readyBeforeString")
            Timber.d("Date: $dateString")
            Timber.d("Order: $orderResponse")


            val bookResponse = try {
                apiService.book(
                    userRepository.getAuthorizedUserIdToken(),
                    orderResponse.orderId,
                    childrenNumber,
                    adultsNumber,
                    readyBeforeString,
                    dateString
                )
            }catch (e: retrofit2.HttpException){
                userRepository.updateIdToken()
                apiService.book(
                    userRepository.getAuthorizedUserIdToken(),
                    orderResponse.orderId,
                    childrenNumber,
                    adultsNumber,
                    readyBeforeString,
                    dateString
                )
            }
            Timber.d("Book: $bookResponse")

            Timber.d("Order id: ${orderResponse.orderId}")
            Timber.d("Card: $card")
            Timber.d("Card exp month: $cardExpMonth")
            Timber.d("Card exp year: $cardExpYear")
            Timber.d("cvv: $cvv")

//            val paymentResponse = try{
//                apiService.payment(
//                    userRepository.getAuthorizedUserIdToken(),
//                    orderResponse.orderId,
//                    card,
//                    cardExpMonth,
//                    cardExpYear,
//                    cvv
//                )
//            }catch (e: retrofit2.HttpException){
//                userRepository.updateIdToken()
//                apiService.payment(
//                    userRepository.getAuthorizedUserIdToken(),
//                    orderResponse.orderId,
////                    card,
//                    4242424242424242,
//                    cardExpMonth,
//                    cardExpYear,
//                    cvv
//                )
//            }
//            Timber.d("Payment: $paymentResponse")

            withContext(Dispatchers.Main){
                orderRepository.clearCart()
                viewState.goToMainPageActivity()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}
package com.example.smokestalks.ui

import android.os.Bundle
import androidx.fragment.app.Fragment

interface FragmentsRouters {
    fun navigateToFragment(fragment: Fragment, bundle: Bundle?)
}
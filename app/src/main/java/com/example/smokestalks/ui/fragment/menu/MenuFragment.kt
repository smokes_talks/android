package com.example.smokestalks.ui.fragment.menu

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smokestalks.R
import com.example.smokestalks.repositories.MenuRepository
import com.example.smokestalks.ui.dialogs.LoginDialogFragment
import kotlinx.android.synthetic.main.menu_fragment.*
import kotlinx.android.synthetic.main.main_activity.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject

class MenuFragment : MvpAppCompatFragment(), MenuView {

    @InjectPresenter
    lateinit var presenter: MenuPresenter

    @ProvidePresenter
    fun provideMenuPresenter() = get<MenuPresenter>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.menu_fragment, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.bottomNavigationView?.visibility = View.VISIBLE
        activity?.titleToolbar?.visibility = View.VISIBLE
    }

    override fun setCategoryAdapter(categoryAdapter: CategoryAdapter) {
        categoriesRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = categoryAdapter
        }
    }

    override fun setSubcategoryAdapter(subcategoryAdapter: SubcategoryAdapter) {
        subcategoriesRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = subcategoryAdapter
        }
    }

    override fun setMenuAdapter(menuAdapter: MenuAdapter) {
        menuRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = menuAdapter
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    LinearLayoutManager.VERTICAL
                )
            )
        }
//        menuRecyclerView.recycledViewPool.setMaxRecycledViews(0, 0)
    }

    override fun requestLogin() {
        val fragmentTransaction = parentFragmentManager.beginTransaction()
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = LoginDialogFragment()
        dialogFragment.show(fragmentTransaction, "dialog")
    }

//    override fun onStart() {
//        super.onStart()
//        presenter.updateMenu()
//    }
}

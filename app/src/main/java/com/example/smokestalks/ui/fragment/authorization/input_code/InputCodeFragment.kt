package com.example.smokestalks.ui.fragment.authorization.input_code

import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.smokestalks.Constants
import com.example.smokestalks.R
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.raycoarana.codeinputview.OnDigitInputListener
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher
import kotlinx.android.synthetic.main.actionbar.view.*
import kotlinx.android.synthetic.main.input_code_fragment.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get
import java.util.concurrent.TimeUnit

class InputCodeFragment : MvpAppCompatFragment(), InputCodeView {

    @InjectPresenter
    lateinit var presenter: InputCodePresenter

    @ProvidePresenter
    fun provideInputCodePresenter() = get<InputCodePresenter>()

    private val mVerificationId by lazy { arguments?.getString(Constants.SEND_ID) }
    private val phoneNumber by lazy { arguments?.getString("phone") }
    private lateinit var smsVerifyCatcher: SmsVerifyCatcher

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.input_code_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        smsVerifyCatcher = SmsVerifyCatcher(activity) { message ->
            val code: String = presenter.parseCode(message) //Parse verification code
            inputCodeView.code = code //set code in edit text
            verifyVerificationCode(code)
            //then you can send verification code to server
        }

        setGradientForErrorText()

        action_bar.backButton.setOnClickListener {
            findNavController().navigate(R.id.action_inputCodeFragment_to_inputNumberFragment)
        }

        inputCodeView.addOnCompleteListener { code ->
            verifyVerificationCode(code.toString())
        }

        inputCodeView.requestFocus()
        inputCodeView.addOnDigitInputListener(object : OnDigitInputListener {
            override fun onInput(inputDigit: Char) {}
            override fun onDelete() {
                codeErrorTextView.visibility = View.INVISIBLE
            }
        })

        sendCodeAgainTextView.setOnClickListener {
            sendVerificationCode(phoneNumber!!)
        }
    }

    fun verifyVerificationCode(code: String) {
        val credential = mVerificationId?.let { PhoneAuthProvider.getCredential(it, code) }
        credential?.let { presenter.signInWithPhoneAuthCredential(it, phoneNumber!!) }
    }

    private fun sendVerificationCode(phoneNumber: String) {
        val options = PhoneAuthOptions.newBuilder(presenter.mAuth)
            .setPhoneNumber(phoneNumber) // Phone number to verify
            .setTimeout(10L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(requireActivity()) // Activity (for callback binding)
            .setCallbacks(mCallBack) // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    private val mCallBack: PhoneAuthProvider.OnVerificationStateChangedCallbacks =
        object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                val code = phoneAuthCredential.smsCode
                if (code != null) {
                    verifyVerificationCode(code)
                } else {
                    findNavController().navigate(R.id.action_inputCodeFragment_to_menuActivity)
                }
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Toast.makeText(
                    context, "Something went wrong, try again later please",
                    Toast.LENGTH_LONG
                ).show()
            }
        }



    private fun setGradientForErrorText(){
        val paint = codeErrorTextView.paint
        val width = paint.measureText(codeErrorTextView.text.toString())
        val textShader: Shader = LinearGradient(
            0f, 0f, width, codeErrorTextView.textSize, intArrayOf(
                Color.parseColor("#F56C46"),
                Color.parseColor("#EEA749")
            ), null, Shader.TileMode.REPEAT
        )
        codeErrorTextView.paint.shader = textShader
    }

    override fun finishActivity() {
        activity?.finish()
    }

    override fun goToInputPersonalDataActivity() {
        findNavController().navigate(R.id.action_inputCodeFragment_to_inputPersonalDataActivity)
    }

    override fun goToMenuActivity() {
        findNavController().navigate(R.id.action_inputCodeFragment_to_menuActivity)
    }

    override fun wrongInputCode() {
        codeErrorTextView.visibility = View.VISIBLE
        inputCodeView.setEditable(true)
    }

    override fun onStart() {
        super.onStart()
        smsVerifyCatcher.onStart()
    }

    override fun onStop() {
        super.onStop()
        smsVerifyCatcher.onStop()
    }
}

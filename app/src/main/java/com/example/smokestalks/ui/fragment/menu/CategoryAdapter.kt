package com.example.smokestalks.ui.fragment.menu

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.smokestalks.R
import com.example.smokestalks.entities.Category
import com.example.smokestalks.entities.Subcategory
import kotlinx.android.synthetic.main.categories_item.view.*
import org.koin.core.KoinComponent

class CategoryAdapter(var chosenCategoryId: Int) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>(), KoinComponent {

    private val categoriesList = mutableListOf<Category>()
    lateinit var categoryListener: CategoryListener
    private var checkedItem: Category? = null

    fun setCategoryList(list: MutableList<Category>) {
        categoriesList.clear()
        categoriesList.addAll(list)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.categories_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return categoriesList.size
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.apply {
            bind(categoriesList[position], categoryListener)
        }
    }

    inner class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        KoinComponent {

        @SuppressLint("ResourceAsColor")
        fun bind(item: Category, categoryListener: CategoryListener) {
            if (chosenCategoryId == item.id) {
                itemView.indicatorImageView.visibility = View.VISIBLE
                itemView.menuTextView.setTextColor(Color.parseColor("#FFCC00"))
                itemView.indicatorImageView.setImageResource(R.drawable.indicator_icon)
                checkedItem = item
            } else {
                itemView.indicatorImageView.visibility = View.GONE
                itemView.menuTextView.setTextColor(Color.GRAY)
            }

            itemView.menuTextView.text = item.name
            itemView.categoriesItem.setOnClickListener {
                categoryListener.notifyCategorySelected(item.id)
                if (checkedItem != item) {
                    itemView.indicatorImageView.visibility = View.VISIBLE
                    itemView.menuTextView.setTextColor(Color.parseColor("#FFCC00"))
                    itemView.indicatorImageView.setImageResource(R.drawable.indicator_icon)
                    if (checkedItem != null) {
                        notifyItemChanged(categoriesList.indexOf(checkedItem!!))
                    }
                    checkedItem = item
                    chosenCategoryId = item.id
                }
            }
        }
    }

    interface CategoryListener {
        fun notifyCategorySelected(categoryId: Int)
    }
}
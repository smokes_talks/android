package com.example.smokestalks.ui.fragment.payment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.smokestalks.R
import com.example.smokestalks.extensions.isValidCard
import com.example.smokestalks.ui.activity.MainPageActivity
import kotlinx.android.synthetic.main.fragment_payment.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get
import timber.log.Timber
import java.util.*


class PaymentFragment : MvpAppCompatFragment(), PaymentView {

    @InjectPresenter
    lateinit var presenter: PaymentPresenter

    @ProvidePresenter
    fun providePaymentPresenter() = get<PaymentPresenter>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adultsNumber = arguments?.getInt("adultsNumber")
        val childrenNumber = arguments?.getInt("childrenNumber")
        val reservationDate = arguments?.getSerializable("reservationDate") as Calendar?
        val hour = arguments?.getInt("hour")
        val minute = arguments?.getInt("minute")
        val timeFormat = arguments?.getInt("timeFormat")
        Timber.d("adultsNumber: $adultsNumber")

        paymentPageBackButton.setOnClickListener {
            activity?.onBackPressed()
        }

        payButton.setOnClickListener {
            val card = cardNumberEditText.text.toString().replace(" ", "")
            if (!card.isValidCard())
                showToast("Недействительный номер карты")
//            if (card.length < 16)
//                showToast("Введите полный номер карты")
            else {
                val expDate = expirationDateEditText.text.toString()
                if (expDate.length < 5)
                    showToast("Введите полный срок действия")
                else {
                    val cardExpMonth = expDate.slice(0..1)
                    val cardExpYear = expDate.slice(3..4)
                    if (cardExpMonth.toInt() > 12)
                        showToast("Недействительный срок действия")
                    else{
                        val cvv = cvvEditText.text.toString()
                        if (cvv.length < 3)
                            showToast("Введите полный код безопасности")
                        else {
                            presenter.makeOrder(
                                adultsNumber!!,
                                childrenNumber!!,
                                reservationDate!!,
                                hour!!,
                                minute!!,
                                timeFormat!!,
                                card.toLong(),
                                cardExpMonth,
                                cardExpYear,
                                cvv.toInt()
                            )
                        }
                    }
                }
            }
        }
    }

    override fun goToMainPageActivity() {
        startActivity(Intent(activity, MainPageActivity::class.java))
    }

    private fun showToast(text: String) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }
}
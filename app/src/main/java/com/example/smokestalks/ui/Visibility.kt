package com.example.smokestalks.ui

import android.view.View

interface Visibility {
    fun hideView()
    fun showView()
}
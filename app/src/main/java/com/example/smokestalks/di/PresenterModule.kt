package com.example.smokestalks.di

import com.example.smokestalks.ui.fragment.authorization.input_code.InputCodePresenter
import com.example.smokestalks.ui.activity.input_personal_data.InputPersonalDataPresenter
import com.example.smokestalks.ui.fragment.menu.MenuPresenter
import com.example.smokestalks.ui.fragment.profile_edit.ProfileEditPresenter
import com.example.smokestalks.ui.fragment.payment.PaymentPresenter
import org.koin.dsl.module

val presenterModule = module {
    factory { MenuPresenter(get()) }
    factory { InputCodePresenter(get(), get()) }
    factory { InputPersonalDataPresenter(get()) }
    factory { ProfileEditPresenter(get()) }
    factory { PaymentPresenter(get(), get(), get()) }
}
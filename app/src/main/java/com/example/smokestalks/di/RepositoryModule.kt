package com.example.smokestalks.di

import com.example.smokestalks.repositories.MenuRepository
import com.example.smokestalks.repositories.OrderRepository
import com.example.smokestalks.repositories.UserRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { UserRepository(get(), get(), get()) }
    single { MenuRepository(get()) }
    single { OrderRepository(get(), get(), get()) }
}
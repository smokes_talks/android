package com.example.smokestalks.di

import androidx.room.Room
import com.example.smokestalks.SmokesAndTalks
import com.example.smokestalks.database.AppDatabase
import org.koin.dsl.module

val databaseModule = module {
    single { provideAppDatabase() }
}

fun provideAppDatabase(): AppDatabase = Room.databaseBuilder(
    SmokesAndTalks.applicationContext(),
    AppDatabase::class.java,
    "database").build()

package com.example.smokestalks.extensions

fun CharSequence.formatPhoneText(): String {
    var phone = ""
    phone += this.slice(0..3)
    phone += " ("
    phone += this.slice(4..5)
    phone += ") "
    phone += this.slice(6..8)
    phone += " "
    phone += this.slice(9..10)
    phone += " "
    phone += this.slice(11..12)
    return phone
}
package com.example.smokestalks.extensions

fun CharSequence.isValidCard(): Boolean {
    val listOfPattern = mutableListOf<Regex>()
    // Amex Card
    listOfPattern.add(Regex("^3[47][0-9]{13}\$"))
    // BCGlobal
    listOfPattern.add(Regex("^(6541|6556)[0-9]{12}\$"))
    // Carte Blanche Card
    listOfPattern.add(Regex("^389[0-9]{11}\$"))
    // Diners Club Card
    listOfPattern.add(Regex("^3(?:0[0-5]|[68][0-9])[0-9]{11}\$"))
    // Discover Card
    listOfPattern.add(Regex("^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})\$"))
    // Insta Payment Card
    listOfPattern.add(Regex("^63[7-9][0-9]{13}\$"))
    // JCB Card
    listOfPattern.add(Regex("^(?:2131|1800|35\\d{3})\\d{11}\$"))
    // KoreanLocalCard
    listOfPattern.add(Regex("^9[0-9]{15}\$"))
    // Laser Card
    listOfPattern.add(Regex("^(6304|6706|6709|6771)[0-9]{12,15}\$"))
    // Maestro Card
    listOfPattern.add(Regex("^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}\$"))
    // Mastercard
    listOfPattern.add(Regex("^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))\$"))
    // Solo Card
    listOfPattern.add(Regex("^(6334|6767)[0-9]{12}|(6334|6767)[0-9]{14}|(6334|6767)[0-9]{15}\$"))
    // Switch Card
    listOfPattern.add(Regex("^(4903|4905|4911|4936|6333|6759)[0-9]{12}|(4903|4905|4911|4936|6333|6759)[0-9]{14}|(4903|4905|4911|4936|6333|6759)[0-9]{15}|564182[0-9]{10}|564182[0-9]{12}|564182[0-9]{13}|633110[0-9]{10}|633110[0-9]{12}|633110[0-9]{13}\$"))
    // Union Pay Card
    listOfPattern.add(Regex("^(62[0-9]{14,17})\$"))
    // Visa Card
    listOfPattern.add(Regex("^4[0-9]{12}(?:[0-9]{3})?\$"))
    // Visa Master Card
    listOfPattern.add(Regex("^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})\$"))
    for (pattern in listOfPattern){
        if (this.matches(pattern))
            return true
    }
    return false
}
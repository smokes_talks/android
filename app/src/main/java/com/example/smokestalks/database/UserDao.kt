package com.example.smokestalks.database

import androidx.room.*
import com.example.smokestalks.entities.User

@Dao
interface UserDao {
    @Query("SELECT * FROM user")
    suspend fun getAllUsers(): MutableList<User>

    @Query("SELECT * FROM user WHERE uid = :uid")
    suspend fun getUserByUid(uid: String) : User?

    @Insert
    suspend fun insertUser(user: User)

    @Update
    suspend fun updateUser(user: User)
}